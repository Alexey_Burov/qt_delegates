#include "delegatecombo.h"
#include <QApplication>

DelegateCombo::DelegateCombo(QObject *parent)
    : ParentDelegate(parent){

    m_parentView = static_cast<QTableView*>(parent);
}

DelegateCombo::~DelegateCombo()
{

}

QWidget *DelegateCombo::createEditor(QWidget *parent,
                                     const QStyleOptionViewItem &option,
                                     const QModelIndex &index) const {

    QComboBox *cbox = new QComboBox(parent);
    connect(cbox, SIGNAL(activated(int)), this, SIGNAL(signalValue(int)));
    connect(cbox, SIGNAL(activated(QString)), this, SLOT(setValue(QString)));
    for (int i = 0; i < m_valuesView.count(); ++i) {
        cbox->addItem(m_valuesView.at(i));
        cbox->setItemData(i, (QVariant(m_valuesView.at(i))), Qt::UserRole);
    }

    cbox->setGeometry(option.rect);
    m_parentView->resizeColumnToContents(index.column());
    return cbox;
}

void DelegateCombo::updateEditorGeometry(QWidget *editor,
                                         const QStyleOptionViewItem &option,
                                         const QModelIndex &index) const
{
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}

void DelegateCombo::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    Q_UNUSED(index)
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    comboBox->setCurrentIndex(m_pos);
}

void DelegateCombo::setModelData(QWidget *editor,
                                 QAbstractItemModel *model,
                                 const QModelIndex &index) const
{
    Q_UNUSED(editor)
    Q_UNUSED(index)
    model->setData(index, QVariant(m_valuesMess.at(m_pos)), Qt::EditRole);
}

void DelegateCombo::setAllVal(QStringList &valView,QStringList &valMess)
{
    m_valuesView = valView;
    m_valuesMess = valMess;
}
void DelegateCombo::setValue(QString v)
{
    m_pos = m_valuesView.indexOf(v);
}
