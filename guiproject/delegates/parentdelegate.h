/**************************************************************************
**  File: parentdelegate.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 20.08.2019
**************************************************************************/

#pragma once

#include <QStyledItemDelegate>

/**
 * @brief The ParentDelegate class родительски класс для DelegateCombo, DelegateSpin, DelegateDoubleSpin, DelegateLineEdit
 */
class ParentDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    /**
     * @brief ParentDelegate конструктор
     * @param parent
     */
    explicit ParentDelegate(QObject *parent = 0);
    virtual ~ParentDelegate();

    /**
     * @brief getValue извлечение текущего значения m_value
     * @return
     */
    int getValue() const;

signals:

public slots:
    /**
     * @brief setValue установка текущего значения m_value
     * @param value
     */
    void setValue(int value);

private:
    int m_value;

};
