#include "parentdelegate.h"

#include <QDebug>

ParentDelegate::ParentDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
    setValue(0);
}

ParentDelegate::~ParentDelegate()
{
//    qDebug() << "delete parent delegate";
}
int ParentDelegate::getValue() const
{
    return m_value;
}

void ParentDelegate::setValue(int value)
{
    m_value = value;
}

