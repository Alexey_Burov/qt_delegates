#include "delegatelineedit.h"
#include <QLineEdit>

DelegateLineEdit::DelegateLineEdit(QObject *parent): ParentDelegate(parent){

}

DelegateLineEdit::~DelegateLineEdit()
{

}

QWidget *DelegateLineEdit::createEditor(QWidget *parent,
    const QStyleOptionViewItem &option,
    const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)
    QLineEdit *le = new QLineEdit(parent);
    connect(le,SIGNAL(textChanged(QString)),this,SIGNAL(signalValue(QString)));

    return le;
}

void DelegateLineEdit::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    QString value = index.model()->data(index, Qt::EditRole).toString();
    QLineEdit *le = static_cast<QLineEdit*>(editor);
    le->setText(value);
}

void DelegateLineEdit::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    QLineEdit *le = static_cast<QLineEdit*>(editor);
    QString value = le->text();
    model->setData(index, value, Qt::EditRole);
}

void DelegateLineEdit::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}
