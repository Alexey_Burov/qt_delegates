#include "delegatedoublespin.h"
#include <QDoubleSpinBox>
#include <QDebug>

namespace {
const int c_min = -999999999;
const int c_max = 999999999;
}


DelegateDoubleSpin::DelegateDoubleSpin(QObject *parent)
    : ParentDelegate(parent)
{
}

DelegateDoubleSpin::~DelegateDoubleSpin()
{

}

QWidget *DelegateDoubleSpin::createEditor(QWidget *parent,
    const QStyleOptionViewItem &option,
    const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)
    QDoubleSpinBox *spb = new QDoubleSpinBox(parent);
    spb->setMinimum(c_min);
    spb->setMaximum(c_max);
    connect(spb,SIGNAL(valueChanged(double)), this, SIGNAL(signalValue(double)));
    return spb;
}

void DelegateDoubleSpin::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    double value = index.model()->data(index, Qt::EditRole).toDouble();
    QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
    spinBox->setValue(value);
}

void DelegateDoubleSpin::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
    spinBox->interpretText();
    double value = spinBox->value();
    model->setData(index, value, Qt::EditRole);
}

void DelegateDoubleSpin::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}
