/**************************************************************************
**  File: delegatespin.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 20.08.2019
**************************************************************************/

#pragma once

#include "parentdelegate.h"

/**
 * @brief The DelegateSpin class класс описывает делегат реализующий селектор целочисленных чисел
 */
class DelegateSpin : public ParentDelegate
{
    Q_OBJECT

public:
    /**
     * @brief DelegateSpin
     * @param parent
     */
    DelegateSpin(QObject *parent = 0);
    ~DelegateSpin();

    /**
     * @brief createEditor переопределенный метод, создаем QSpinBox
     * @param parent    родитель
     * @param option    стиль
     * @param index     привязка к элементу таблицы
     * @return          QSpinBox
     */
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const;

    /**
     * @brief setEditorData установка позиции combobox (значения)
     * @param editor        QWidget
     * @param index         привязка к элементу таблицы
     */
    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    /**
     * @brief setModelData  установка строкового значения
     * @param editor        QWidget
     * @param model         model
     * @param index         привязка к элементу таблицы
     */
    void setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const;

    /**
     * @brief updateEditorGeometry установка геометрии
     * @param editor    QWidget
     * @param option    стиль
     * @param index     привязка к элементу таблицы
     */
    void updateEditorGeometry(QWidget *editor,const QStyleOptionViewItem &option, const QModelIndex &index) const;

signals:
        void signalValue(int);

};
