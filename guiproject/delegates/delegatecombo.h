/**************************************************************************
**  File: delegatecombo.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 20.08.2019
**************************************************************************/

#pragma once

#include <QStyledItemDelegate>
#include <QObject>
#include <QComboBox>
#include <QTableView>
#include <QDebug>
#include "parentdelegate.h"

/**
 * @brief The DelegateCombo class класс описывает делегат реализующий выпадающий список
 */
class DelegateCombo : public ParentDelegate
{
    Q_OBJECT

public:
    //! Constructor.
    DelegateCombo(QObject *parent = 0);

    ~DelegateCombo();

    /**
     * @brief createEditor переопределенный метод, создаем QComboBox
     * @param parent    родитель
     * @param option    стиль
     * @param index     привязка к элементу таблицы
     * @return          QComboBox
     */
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    /**
     * @brief updateEditorGeometry установка геометрии
     * @param editor    QWidget
     * @param option    стиль
     * @param index     привязка к элементу таблицы
     */
    void updateEditorGeometry(QWidget *editor,const QStyleOptionViewItem &option, const QModelIndex &index) const;

    /**
     * @brief setEditorData установка позиции combobox (значения)
     * @param editor        QWidget
     * @param index         привязка к элементу таблицы
     */
    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    /**
     * @brief setModelData  установка строкового значения
     * @param editor        QWidget
     * @param model         model
     * @param index         привязка к элементу таблицы
     */
    void setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const;

    /**
     * @brief setAllVal установка двух списков соответствий
     * @param valView   список для отображения
     * @param valMess   список для сообщения
     */
    void setAllVal(QStringList &valView, QStringList &valMess);

    /**
     * @brief m_pos текущая позиция
     */
    int m_pos;

signals:
    void signalValue(int);

public slots:
    void setValue(QString v);

private:
    QStringList m_itemList;
    QTableView *m_parentView;
    QStringList m_valuesView;
    QStringList m_valuesMess;
};
