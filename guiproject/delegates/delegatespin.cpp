#include "delegatespin.h"
#include <QSpinBox>

namespace {
const int c_min = -999999999;
const int c_max = 999999999;
}

DelegateSpin::DelegateSpin(QObject *parent)
    : ParentDelegate(parent)
{
}

DelegateSpin::~DelegateSpin()
{

}

QWidget *DelegateSpin::createEditor(QWidget *parent,
    const QStyleOptionViewItem &option,
    const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)

    QSpinBox *spb = new QSpinBox(parent);
    spb->setMinimum(c_min);
    spb->setMaximum(c_max);
    connect(spb, SIGNAL(valueChanged(int)), this, SIGNAL(signalValue(int)));
    return spb;
}

void DelegateSpin::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    int value = index.model()->data(index, Qt::EditRole).toInt();

    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
    spinBox->setValue(value);
}

void DelegateSpin::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
    spinBox->interpretText();
    int value = spinBox->value();

    model->setData(index, value, Qt::EditRole);
}

void DelegateSpin::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}
