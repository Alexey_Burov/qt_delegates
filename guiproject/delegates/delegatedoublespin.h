/**************************************************************************
**  File: delegatedoublespin.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 20.08.2019
**************************************************************************/

#pragma once

#include "parentdelegate.h"

/**
 * @brief The DelegateDoubleSpin class класс описывает делегат реализующий селектор вещественный чисел
 */
class DelegateDoubleSpin : public ParentDelegate
{
    Q_OBJECT

public:
    /**
     * @brief DelegateDoubleSpin конструктор
     * @param parent
     */
    DelegateDoubleSpin(QObject *parent = 0);
    ~DelegateDoubleSpin();

    /**
     * @brief createEditor переопределенный метод, создаем QDoubleSpinBox
     * @param parent    родитель
     * @param option    стиль
     * @param index     привязка к элементу таблицы
     * @return          QDoubleSpinBox
     */
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const;

    /**
     * @brief setEditorData установка позиции combobox (значения)
     * @param editor        QWidget
     * @param index         привязка к элементу таблицы
     */
    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    /**
     * @brief setModelData  установка строкового значения
     * @param editor        QWidget
     * @param model         model
     * @param index         привязка к элементу таблицы
     */
    void setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const;

    /**
     * @brief updateEditorGeometry установка геометрии
     * @param editor    QWidget
     * @param option    стиль
     * @param index     привязка к элементу таблицы
     */
    void updateEditorGeometry(QWidget *editor,const QStyleOptionViewItem &option, const QModelIndex &index) const;

signals:
        void signalValue(double);
        void editingFinished();

};
