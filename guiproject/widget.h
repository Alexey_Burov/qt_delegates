/**************************************************************************
**  File: widget.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 19.08.2019
**************************************************************************/

#pragma once

#include <QWidget>
#include <QSqlTableModel>
#include "services/send.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:

    /**
     * @brief Widget конструктор
     * @param parent
     */
    explicit Widget(QWidget *parent = 0);
    ~Widget();



private:

    Ui::Widget *ui;
    QSqlTableModel *m_model;

    /**
     * @brief initModel первоначальная инициализация модели и установка ее на tableView
     */
    void initModel();

    /**
     * @brief initDelegateSpin инициализация делегата целочисленного ввода
     * @param column           номер колонки
     * @param name             objectName
     */
    void initDelegateSpin(int column, QString name);

    /**
     * @brief initDelegateCombo инициализация делегата выпадающего списка
     * @param column            номер колонки
     * @param name              objectName
     * @param previewValues     список на отображение
     * @param messageValues     список значения в сообщении
     */
    void initDelegateCombo(int column, QString name, QStringList previewValues, QStringList messageValues);

    /**
     * @brief initView  инициализация gui
     */
    void initView();

    /**
     * @brief send класс отправки по сети
     */
    Send m_send;

signals:
    /**
     * @brief sendMessage отправка сообщений
     */
    void sendMessage(char*,int);

private slots:

    /**
     * @brief setMessage инициализация отправки сообщений
     */
    void setMessage();

    /**
     * @brief removeMessage удаление сообщения из tableView
     */
    void removeMessage();

    /**
     * @brief addMessage добавление сообщения в tableView
     */
    void addMessage();
};
