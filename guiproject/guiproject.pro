#-------------------------------------------------
#
# Project created by QtCreator 2019-08-19T13:07:33
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = guiproject
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    delegates/parentdelegate.cpp \
    delegates/delegatespin.cpp \
    delegates/delegatelineedit.cpp \
    delegates/delegatedoublespin.cpp \
    delegates/delegatecombo.cpp \
    services/send.cpp \
    services/recive.cpp

HEADERS  += widget.h \
    delegates/parentdelegate.h \
    delegates/delegatespin.h \
    delegates/delegatelineedit.h \
    delegates/delegatedoublespin.h \
    delegates/delegatecombo.h \
    services/dbconnector.h \
    services/send.h \
    services/recive.h

FORMS    += widget.ui
