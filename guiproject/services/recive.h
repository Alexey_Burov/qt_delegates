/**************************************************************************
**  File: send.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 20.08.2019
**************************************************************************/

#pragma once

#include <QObject>
#include <QUdpSocket>
#include <QTimer>

/**
 * @brief The Recive class класс приема сетевых сообщений
 */
class Recive : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Recive конструктор
     * @param parent
     */
    explicit Recive(QObject *parent = 0);
    ~Recive();

signals:

    /**
     * @brief reciveMessage отправка принятого сообщения для дальнейшей обработки
     */
    void reciveMessage(char*,int);

private slots:
    /**
     * @brief readPendingDatagrams слот приема по UDP протоколу
     */
    void readPendingDatagrams();

private:
    QUdpSocket *m_udpSocket;

};
