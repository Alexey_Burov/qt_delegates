/**************************************************************************
**  File: dbconnector.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 19.08.2019
**************************************************************************/

#pragma once

#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>


namespace {
const QString driver = "QSQLITE";
const QString dbName = "../data/db";
}

/**
 * @brief Widget::initDBConnection инициализация БД QSQLITE файл db в директории ../data/
 */
static bool initDBConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase(driver);
    db.setDatabaseName(dbName);
    if (!db.open()) {
        qCritical() << db.lastError().text();
        return false;
    }
    return true;
}
