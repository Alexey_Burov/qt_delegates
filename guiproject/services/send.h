/**************************************************************************
**  File: delegatecombo.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 20.08.2019
**************************************************************************/

#pragma once

#include <QObject>
#include <QUdpSocket>
#include <QTimer>

/**
 * @brief The Send class класс отправки сетевых сообщений
 */
class Send : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Send конструктор
     * @param parent
     */
    explicit Send(QObject *parent = 0);
    ~Send();

signals:
    /**
     * @brief netstatfail сигнал контроля состояния сети
     */
    void netstatfail(bool);

public slots:

    /**
     * @brief sendMessage отправка сообщения
     * @param mess        буфер сообщения
     * @param size        размер буфера
     */
    void sendMessage(char* mess,int size);

private:
    QUdpSocket *m_udpSocket;
};
