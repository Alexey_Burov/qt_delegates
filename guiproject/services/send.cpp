#include "send.h"
#include <QSqlQuery>

namespace  {
static const QString c_ip = "172.13.1318";
static const int c_port = 3456;
}

Send::Send(QObject *parent) :
    QObject(parent)
{
    m_udpSocket = new QUdpSocket;
}

Send::~Send()
{
    delete m_udpSocket;
}

void  Send:: sendMessage(char*mess,int size)
{
    Q_UNUSED(size)
    if(m_udpSocket->writeDatagram(mess,strlen(mess),QHostAddress(c_ip),c_port) == -1)
        emit netstatfail(true);
    else
        emit netstatfail(false);
}
