#include "recive.h"

namespace  {
const int c_port = 3456;
}

Recive::Recive(QObject *parent) :
    QObject(parent)
{
    m_udpSocket = new QUdpSocket(this);
    m_udpSocket->bind(c_port,QUdpSocket::ReuseAddressHint);
    connect(m_udpSocket, SIGNAL(readyRead()),
            this, SLOT(readPendingDatagrams()));
}

Recive::~Recive()
{
    delete m_udpSocket;
}

void Recive::readPendingDatagrams()
{
    while (m_udpSocket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(m_udpSocket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;
        m_udpSocket->readDatagram(datagram.data(), datagram.size(),
                                &sender, &senderPort);
        emit reciveMessage(datagram.data(), datagram.size());
    }
}
