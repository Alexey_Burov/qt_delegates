/**************************************************************************
**  File: main.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 19.08.2019
**************************************************************************/

#include <QApplication>

#include "widget.h"
#include "services/dbconnector.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ::initDBConnection();

    Widget w;
    w.show();

    return a.exec();
}
