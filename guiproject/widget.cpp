/**************************************************************************
**  File: widget.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 19.08.2019
**************************************************************************/

#include "widget.h"
#include "ui_widget.h"

#include "delegates/delegatecombo.h"
#include "delegates/delegatespin.h"
#include "delegates/delegatelineedit.h"
#include "delegates/delegatedoublespin.h"

namespace {
const QString c_sys = "sys";
const QString c_name = "name";
const QString c_type = "type";
const QString c_range = "range";
const QString c_prefix = "$CSXRR,";
const QString c_country = "country";
const QString c_tableNameMeans = "means";
}

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    initView();
    connect(ui->btnAdd,SIGNAL(clicked()),this,SLOT(addMessage()));
    connect(ui->btnRemove,SIGNAL(clicked()),this,SLOT(removeMessage()));
    connect(ui->btnSend,SIGNAL(clicked()),this,SLOT(setMessage()));
    connect(this,SIGNAL(sendMessage(char*,int)),&m_send,SLOT(sendMessage(char*,int)));

}

void Widget::initModel()
{
    m_model = new QSqlTableModel;
    m_model->setTable(c_tableNameMeans);
    ///изменять значения в БД при смене ячеек
    m_model->setEditStrategy(QSqlTableModel::OnFieldChange);
    m_model->select();
    m_model->sort(0,Qt::AscendingOrder);
    ui->tableView->setModel(m_model);

    QSqlQueryModel heads;
    heads.setQuery("SELECT * FROM heads");
    for (int i = 0; i < heads.columnCount(); ++i)
        m_model->setHeaderData(i,Qt::Horizontal,heads.data(heads.index(0,i)).toString().toUtf8());
    ui->tableView->resizeColumnsToContents();
}

void Widget::initDelegateSpin(int column, QString name){
    DelegateSpin* spin = new DelegateSpin;
    spin->setObjectName(name);
    ui->tableView->setItemDelegateForColumn(column, spin);
}

void Widget::initDelegateCombo(int column, QString name,
                               QStringList previewValues, QStringList messageValues)
{
    DelegateCombo *combo = new DelegateCombo(ui->tableView);
    combo->setAllVal(previewValues,messageValues);
    combo->setObjectName(name);
    ui->tableView->setItemDelegateForColumn(column, combo);
}

void Widget::initView()
{
    initModel();
    ui->tableView->hideColumn(0);
    QSqlQueryModel name;
    name.setQuery("SELECT * FROM name");
    QStringList nameView;
    QStringList nameMess;
    QStringList systemList;
    QStringList typeList;
    QStringList countryName;
    QStringList countryMess;
    for (int i = 0; i < name.rowCount(); ++i) {
        nameView << name.data(name.index(i,0)).toString();
        nameMess << name.data(name.index(i,1)).toString();
        systemList << name.data(name.index(i,2)).toString();
        typeList << name.data(name.index(i,3)).toString();
        countryName << name.data(name.index(i,4)).toString();
        countryMess << name.data(name.index(i,5)).toString();
    }
    initDelegateCombo(1,c_name,nameView,nameMess);
    initDelegateCombo(2,c_sys,systemList,systemList);
    initDelegateCombo(3,c_type,typeList,typeList);
    initDelegateCombo(4,c_country,countryName,countryMess);
    initDelegateSpin(5, c_range);
    initDelegateSpin(6, c_range);
    initDelegateSpin(7, c_range);
    initDelegateSpin(8, c_range);
}

void Widget::setMessage()
{
    QByteArray mess;
    mess.append(c_prefix);
    for (int i = 0; i < m_model->columnCount(); ++i)
        mess.append(ui->tableView->model()->
                    index(ui->tableView->currentIndex().row(),i ).
                    data().toByteArray()).append(',');

    mess.append('S');
    mess.append(0x00D);
    mess.append(0x00A);
    mess.chop(2);
    ui->label->setText(QString(mess));
    emit sendMessage(mess.data(),mess.size());

}

void Widget::addMessage()
{
    m_model->insertRow(m_model->rowCount());
    QModelIndex index = m_model->index(m_model->rowCount() -1,0);
    m_model->setData(index,m_model->rowCount() -1);
    index = m_model->index(m_model->rowCount() -1,1);
    m_model->submit();
}

void Widget::removeMessage()
{
    m_model->removeRow(ui->tableView->currentIndex().row());
    m_model->submit();
    m_model->sort(0,Qt::AscendingOrder);
}

Widget::~Widget()
{
    delete ui;
}
