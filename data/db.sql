BEGIN TRANSACTION;
CREATE TABLE "name" (
	`name`	TEXT,
	`mess`	TEXT,
	`sys`	TEXT,
	`type`	TEXT,
	`country`	TEXT,
	`coord`	TEXT
);
INSERT INTO `name` (name,mess,sys,type,country,coord) VALUES ('Cavalier','CVL','PARCS','FPQ','США(Северная Дакота)','48°43′28″N 97°53′58″W'),
 ('Clear','CLR','PAVE','FPS','США(Аляска)','64°18′01″N 149°11′27″W'),
 ('Tule','TUL','BMEWS','GBR','Дания','76°34′13″N 68°17′57″W'),
 ('Varde','VRD','*','AN','Норвегия (Шпицберген)','70°22′01″N31°07′39″W');
CREATE TABLE "means" (
	`id`	INTEGER,
	`name`	TEXT,
	`system`	TEXT,
	`type`	TEXT,
	`country`	TEXT,
	`range`	INTEGER,
	`azimuth`	INTEGER,
	`bisector`	INTEGER,
	`angle`	INTEGER
);
INSERT INTO `means` (id,name,system,type,country,range,azimuth,bisector,angle) VALUES (0,'CVL','PARCS','FPQ','48°43′28″N 97°53′58″W',3300,140,8,48),
 (1,'TUL','BMEWS','FPS','76°34′13″N 68°17′57″W',5556,240,57,90),
 (2,'CLR','PAVE','AN','70°22′01″N31°07′39″W',2500,180,19,43),
 (3,'VRD','PARCS','FPQ','48°43′28″N 97°53′58″W',1200,120,7,52);
CREATE TABLE "heads" (
	`id`	TEXT,
	`name`	TEXT,
	`system`	TEXT,
	`type`	TEXT,
	`country`	TEXT,
	`range`	TEXT,
	`azimuth`	TEXT,
	`bisector`	TEXT,
	`angle`	TEXT
);
INSERT INTO `heads` (id,name,system,type,country,range,azimuth,bisector,angle) VALUES ('id','Наименование','Система','Тип РЛС','Страна
расположения','Дальность, км ','Общий азимут
сектора','Направление
биссектрисы
сектора','Угол места
сектора');
COMMIT;
